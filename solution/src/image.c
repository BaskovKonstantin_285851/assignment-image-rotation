#include "image.h"

struct image image_init(uint32_t width, uint32_t height){
	struct image img = {0};
	img.width = width;
	img.height = height;
	img.data = malloc (sizeof(struct pixel)*width*height);
	if (img.data == NULL)
	{
		printf("Что-то пошло не так при выделении памяти");

	}
	return img;
}

void image_destroy(struct image* image){
	if(image){
		if(image->data)
			free(image->data);
		free(image);
	}
}
