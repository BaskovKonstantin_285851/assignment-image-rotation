#include "bmp.h"
#include "image.h"
#include <stdbool.h>

static bool check_type(struct bmp_header header){	
	return header.bfType == 19778;
}

static uint8_t  calculate_padding(uint32_t width){
	return (4 - ( sizeof ( struct pixel ) * width ) % 4) % 4;
}

static bool read_pixels(FILE *file, uint32_t offset, uint32_t width, uint32_t height,
		struct pixel* data){
	uint8_t padding = calculate_padding(width);
	fseek(file, offset, SEEK_SET);
	struct pixel* current = data;
	for(uint32_t i = 0; i < height; ++i){
		size_t read = fread(current, sizeof(struct pixel), width, file);
		if(!read){
			return false;}
		fseek(file, padding, SEEK_CUR);
		current+=read;
	}
	return true;
}

enum read_status from_bmp(FILE *in, struct image *img){
	if(!in){
		return (READ_INVALID_FILE);}
	if(!img){
		return READ_INVALID_POINTER;}
	struct bmp_header header = {0};
	size_t read = fread(&header, sizeof(struct bmp_header), 1, in);
	if(!read)
		return (READ_INVALID_HEADER);
	if(!check_type(header)){
		return (READ_INVALID_SIGNATURE);}
	uint32_t height = header.biHeight;
	uint32_t width = header.biWidth;
	uint32_t offset = header.bOffBits;
	*img = image_init(width, height);
	if(!read_pixels(in, offset, width, height, img->data)){
		return (READ_INVALID_BITS);
	}
	return (READ_OK);
}

static bool build_header(uint32_t width, uint32_t height,
	const struct pixel* data, struct bmp_header *header){
	bool status = (!data || width == 0 || height == 0|| !header);
	if(status)
		return false;
	header->bfType = 19778;
	uint32_t sizeWithoutPadding = sizeof(struct pixel)*width*height;
	uint32_t sizeOfPadding = calculate_padding(width)*height;
	header->biSizeImage = sizeWithoutPadding + sizeOfPadding;
	header->bfileSize = sizeof(struct bmp_header) + header->biSizeImage;
	header->bfReserved = 0;
	header->bOffBits = 54;
	header->biSize = 40;
	header->biWidth = width;
	header->biHeight = height;
	header->biPlanes = 1;
	header->biBitCount = 24;
	header->biCompression = 0;
	header->biXPelsPerMeter = 2834;
	header->biYPelsPerMeter = 2834;
	header->biClrUsed = 0;
	header->biClrImportant = 0;
	return true;
}

static bool write_header(FILE *file, struct bmp_header* header){
	return fwrite(header, sizeof(struct bmp_header), 1, file);
}

static bool write_pixels(FILE *file,uint32_t offset, uint32_t width, uint32_t height, 
		const struct pixel* data){
	int answer = fseek(file, offset, SEEK_SET);
	if (answer == 0)
	{
		printf("Что-то пошло не так при выполнении команды fseek");
	}
	uint8_t padding = calculate_padding(width);
	uint8_t zero[4] = {0,0,0,0};
	for(uint32_t i = 0; i < height; ++i){
		size_t written = fwrite(data, sizeof(struct pixel), width, file);

		size_t size_to_write = fwrite(&zero, 1, padding, file);
		if (size_to_write == 0)
		{
			return false;
		}
		data+=written;
	}
	return true;
}

enum write_status to_bmp(FILE *out, const struct image *img){
	if(!img)
		return (WRITE_INVALID_POINTER);
	if(!out)
		return (WRITE_INVALID_FILE);
	uint32_t height = img->height;
	uint32_t width = img->width;
	struct pixel* data = img->data;
	struct bmp_header header = {0};
	if(!build_header(width, height, data, &header)) {
		return (WRITE_INVALID_IMAGE);
	}
	if(!write_header(out, &header)){
		return (WRITE_ERROR);
	}
	if(!write_pixels(out, header.bOffBits, width, height, data)){
		return (WRITE_ERROR);
	}
	return (WRITE_OK);
}
