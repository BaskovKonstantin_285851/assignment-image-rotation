#include "bmp.h"
#include "file.h"
#include "image.h"
#include "rotation.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>



int main( int argc, char** argv ) {
    if(argc < 3){
        printf("Мало аргументов");
	    return 1;
    }
    enum open_status open_status;
    enum close_status close_status;
    enum read_status read_status;
    enum write_status write_status;



    FILE* original_image = {0};
    FILE* new_image = {0};
    open_status = open_for_read(argv[1], &original_image);
    if(open_status != OPEN_OK){
        printf("Что-то пошло не так при открытии");
	    return 1;
    }
    struct image *image = malloc(sizeof(struct image));
    read_status = from_bmp(original_image, image);
    if(read_status != READ_OK){
        printf("Что-то пошло не так при чтении");
        close(original_image);
        image_destroy(image);
	    return 1;
    }
    close_status = close(original_image);
    if(close_status != CLOSE_OK){
        printf("Что-то пошло не так при закрытии");
        image_destroy(image);
	    return 1;
    }
    
    open_status = open_for_write(argv[2], &new_image);
    if(open_status != OPEN_OK){
        printf("Что-то пошло не так при открытии");
	    image_destroy(image);
	    return 1;
    }
    struct image *n_image = malloc(sizeof(struct image));
    *n_image = rotate(*image);
    image_destroy(image);
    write_status =to_bmp(new_image, n_image);
    if(write_status != WRITE_OK){
        printf("Что-то пошло не так при записи");
	    close(new_image);
        image_destroy(n_image);
	    return 1;
    }
    close_status = close(new_image);
    image_destroy(n_image);
    if(close_status != CLOSE_OK){
        printf("Что-то пошло не так при закрытии");
	    return 1;
    }
    
    return 0;
}
