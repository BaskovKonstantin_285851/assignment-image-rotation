#include "rotation.h"
#include "image.h"

static void rotation(const struct pixel* data, uint32_t oldWidth, uint32_t oldHeight, struct pixel* newData){
	for(size_t i = (oldWidth*oldHeight)-oldWidth; i < oldWidth*oldHeight; i++)
		for(size_t j = i; j < oldHeight*oldWidth; j-=oldWidth){
			*newData = data[j];
			newData += 1;		
	}
}

struct image rotate(const struct image source){
	uint32_t width = source.width;
	uint32_t height = source.height;
	struct pixel* oldData = source.data;
	struct image img = image_init(height, width);
	struct pixel* newData = img.data;
	rotation(oldData, width, height, newData);
	
	return img;
}
